#!/usr/bin/env python3

from random import randint

from result import DiceResult

DICES = {}

def get_dice(faces):
    dice = DICES.get(faces)
    if not dice:
        dice = Dice(faces)
        DICES[faces] = dice
    return dice

class Dice:

    def __init__(self, faces):
        self.faces = faces
        self.last_roll = None

    def roll(self, dices_qty):
        return DiceResult(
            [randint(1, self.faces) for _ in range(dices_qty)]
        )
