#!/usr/bin/env python3

from job import Job

class Rogue(Job):

    health = 6
    defense = 1
    strength = 0
    initiative = 2
