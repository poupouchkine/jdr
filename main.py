#!/usr/bin/env python3

from random import choice
from all_against_regis import AllAgainstRegis
from character import Character
from human import Human
from dwarf import Dwarf
from elf import Elf
from sorcerer import Sorcerer
from warrior import Warrior
from rogue import Rogue

if __name__ == "__main__":
    team1 = [
        Character("roger", Rogue(), Dwarf()),
        Character("pierre", Sorcerer(), Human()),
        Character("roselyne", Warrior(), Dwarf()),
        Character("warren regis", Warrior(), Elf()),
        Character("bob regis", Rogue(), Human()),
        Character("michel regis", Sorcerer(), Dwarf()),
    ]
    fight = AllAgainstRegis(team1)
    fight.run()
