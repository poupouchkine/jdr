#!/usr/bin/env python3

from job import Job

class Warrior(Job):
    
    defense = 0
    strength = 1
    health = 8
    initiative = -1
