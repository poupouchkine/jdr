#!/usr/bin/env python3

from race import Race

class Elf(Race):

    defense = 0
    strength = 0
    health = -1
    initiative = 3
