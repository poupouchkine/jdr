#!/usr/bin/env python3

from job import Job

class Sorcerer(Job):

    health = 4
    defense = 1
    strength = 1
    initiative = 0
