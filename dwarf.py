#!/usr/bin/env python3

from race import Race

class Dwarf(Race):

    defense = 2
    strength = 1
    health = 2
    initiative = -3
