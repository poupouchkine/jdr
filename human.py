#!/usr/bin/env python3

from race import Race

class Human(Race):

    defense = 1
    strength = 2
    health = 0
    initiative = -1
