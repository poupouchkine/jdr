#!/usr/bin/env python3

from dice import get_dice

MODES = ["normal", "defense"]
DIFFICULTY = 3
PRIORITY_FACES = 20

class Character:

    def __init__(self, name, job, race):
        self.job = job
        self.race = race
        self.mode = "normal"
        self.level = 1
        self.set_health()
        self.set_strength()
        self.set_defense()
        self.name = name
        self.actions = {
            "attack": self.attack,
            "defend": self.defend,
            "wait": self.wait,
        }

    @property
    def priority(self):
        modifier = self.job.initiative + self.race.initiative
        return get_dice(20).roll(1).first + modifier

    def set_strength(self):
        self.strength = self.job.strength + self.race.strength

    def set_health(self):
        faces = self.job.health + self.race.health
        dice = get_dice(faces)
        self.health = self.level + dice.roll(1).first

    def set_defense(self):
        self.defense = self.job.defense + self.race.defense

    @property
    def is_dead(self):
        return self.health <= 0

    def attack(self, opponent):
        print(f"{self.name} attacks {opponent.name}")
        self.mode = "normal"
        d6 = get_dice(6)
        success_roll = d6.roll(1).mean
        difficulty = DIFFICULTY + opponent.get_defense()
        print(f"success roll {success_roll}; difficulty {difficulty}")
        if success_roll < difficulty:
            if success_roll == 1:
                print("EPIC FAIL !!!")
            print("attack missed")
            return
        print("attack successful")
        dmg = 1 + self.strength
        if success_roll == 6:
            print("CRITICAL !!!")
            dmg *= 2
        opponent.takedmg(dmg)

    def defend(self):
        print(f"{self.name} defends")
        self.mode = "defense"

    def levelup(self):
        self.level += 1

    def takedmg(self, dmg):
        self.health -= dmg
        print(f"{self.name} takes {dmg} dmg, new hp {self.health}")

    def wait(self):
        print(f"{self.name} waits")

    def get_defense(self):
        res = self.defense
        if self.mode == "defense":
            res += 1
        return res
