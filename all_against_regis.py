#!/usr/bin/env python3

from fight import Fight
from sorcerer import Sorcerer
from dwarf import Dwarf
from character import Character

class AllAgainstRegis(Fight):

    def __init__(self, team):
        team2 = [Character("regis", Sorcerer(), Dwarf())]
        super().__init__(team, team2)
